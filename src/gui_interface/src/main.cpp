#include <QApplication>
#include <QCommandLineParser>

#include "DiceRollerWindow.h"

int main(int argc, char** argv)
{
  QApplication app(argc, argv);

  QCoreApplication::setApplicationName("Dice Roller");
  QCoreApplication::setApplicationVersion("1.0.0");
  QCoreApplication::setOrganizationName("Cheesy Software");

  DiceRollerWindow window;
  window.show();

  return app.exec();
}
