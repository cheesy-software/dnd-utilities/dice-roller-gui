#ifndef CHEESY_SOFTWARE_DICE_ROLLER_WINDOW_H
#define CHEESY_SOFTWARE_DICE_ROLLER_WINDOW_H

#include <memory>

#include "ui_DiceRollerWindow.h"

class DiceRollerWindow : public QMainWindow
{
  private:
    std::unique_ptr<Ui_DiceRollerWindow> m_ui;

  public:
    DiceRollerWindow(QWidget* parent = nullptr);

  signals:

  public slots:

};

#endif
